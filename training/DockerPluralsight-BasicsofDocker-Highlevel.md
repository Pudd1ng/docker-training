# Docker Plural sight - Basics of Docker - High level:

### C1: What are containers?

##### Summary:
Physical servers are expensive and involve wasted time, money and expertise.
Virtulisation removes much of the waste in terms of time but you are still left over with lots of OS overhead.

Containers remove the OS overhead and are quicker then virtulisation. 

### C2: What is docker?
Docker is to containers what vmware is to virtulisation. 

Docker Inc. Is the company behind the Docker OS software. The software runs under Apache License 2.0. The core docker components are written in Golang and a major point release is actively pushed every 2 months.

Docker hub is the global image repository that holds all images official publicly available. Over 5 million pulls a day with 240k repositories stored.

The OCI (Open Container Initiative) Core OS competing standard for Docker. Created due to the company thinking the initial Daemon was bloated. To stop competing standard the OCI was created. Its a governing body that standardizes container format and container runtime. Ensuring that containers remain vendor and platform neutral. Operates under the Linux foundation.

##### Summary:

* Docker inc creates docker os software as well as docker enterprise
* Part of the OCI along with core OS to govern the standard of containers.
* Docker hub is the global image repository and container registry.

### C3: What kind of work will containers do?

Great for modern cloud native apps. As new apps can be designed with containers in mind. They work great with stateless modern apps.

They dont work well with monolithic old apps as its harder to scale them. Stateful persisted data applications are possible to containerise though. Restarting a docker container will mean it still retains its data. You only lose persistent data if you destroy the container. You can also persist data across volumes even if you destroy the container. 

##### Summary:
* Docker containers work with both stateful and stateless apps. but naturally excel with stateless apps.
* The docker storage backend is now pluggable allowing stateful containers to be portable and snapshotted.

### C4: Docker hub and container registries

Contaienr registrys/Image registrys are quickly becoming the appstores of enterprise IT

They are basically places to store and retrive container images. Docker hub being the official registry but others exsist

Registry security falls under public and private repos with standard RBAC security protocols.

Container registrys are becoming part of the CI/CD pipeline

Softwarechange -> SW Repo -> Testing -> Container registry -> Deployment -> Cloud platform

##### Summary:
* Container registries are container app stores. You can download exsisting images or push your own
* Docker hub is the main container registry.
* Registries contain multiple repos that store our images they can be secured privately or publicly. 
* Repos themselves can be public or in house and private for enterprise solutions. The DTC ensures registys are kept high quality.

### C5: Docker ready for production?

Docker Engine 1.10 higher is considered high enough to be used in production settings. Still the engine comes in 3 channels. Experimental, Stable, Commercially Supported

Experimental : Bleeding edge, nightly.
Stable : Releases every two months
Commerically stable : Releases every 6 months only uses stable configurations. Also contains commerical support. 

Docker Swarm has also reached version 1.0 so its stable enough to be used in production. Swarm is dockers clustering management tool.

Docker Content Trust ensures Security by allowing client side signing of remote registrys to ensure the image secure, the same and stable.

##### Offerings from docker:

    Builds:         Ship:                       Run:

    In the cloud:	Swarm, Docker Engine		Docker hub, Tutum
				
    On prem:		Swarm, Docker Engine        Docker Universal Control Plane
				    Docker Trusted Registry 		                


##### Summary:

* Anything 1.0 is considered by docker.inc to be production ready.
* Commerical support is available as well.

### C6: Container orchestration

Apps compromise of multiple parts and multiple containers. Container orchestration can be used to compose all of htese parts into an app. 

e.g.

Load Balancer, Web Servers, Auth, Search broker, Logging, K/V store, Db Backend.

the requirement is to have a pool of hosts with the right containers running on the right hosts.

1. Define application
2. Provision infrastructure
3. Deploy the app

Container orchestration can manage the app dependencies, ensuring each component comes  up in the right order. It can also scale based on usage. 

Docker Machine can provision docker hosts and engines
Docker compose can compose multi-container apps
Docker Swarm schedules contaienrs over multiple docker enginges
Tutum can be used as a UI to manage all of this.

Openstack magnum, coreOS fleet, Kubernetes. All of this tool can be used for container orchestration.

##### Summary:
* One app server and one app component per container.
* Orchestration removes the need for managing dependencies and start up for these services manually.
* The orchestration ecosystem strechtes across multiple tools e.g. k8s, openstack.


