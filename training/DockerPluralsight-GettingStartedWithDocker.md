# Docker Plural sight - Getting Started with Docker:

Remember that the server OS is the only OS you can spin containers up in. If your client is windows and your server is Native Linux you will only be able to spin up Linux containers.

### C1: Installing Docker

Installation on a windows machine: Docker for windows
Currently supports windows 10 64 bit - Test and Dev Usecases.
The install will give you a hyper-V VM running linux with docker installed. Because its running linux behind the scene it will only be able to run windows container.

1. Ensure hyper-v is installed on programs and features. Restart if needed.
2. Grab the InstallDocker.msi from the site or from a private repo.
3. Run the msi and install docker for windows.
4. If the install ran correctly you should be able to open powershell or a prompt to run docker version
5. Notice the client is running windows but the server itself is on linux.

Installing on a mac machine: Docker for mac
Mac hardware from 2010 or later for memory management.
OS.X.10.10.3 YOSEMITE or later

1. Grab the Docker.dmg from the site or a private repo.
2. Run the DMG file and install into applications.
3. Open the applications folder and start up docker.
4. Similar to windows you should see a whale in the menu bar.
5. Open a terminal window and check for docker version
6. Notice the client is running darwin but the server again is on linux. 

Installing docker on windows server:

1. Open up a powershell windows and install the containers feature Install-WindowsFeature containers
2. Restart if needed
3. Create a folder called docker in program files. 
4. Grab the Daemon and client binaries from the site or a private repo.
5. Copy both the executables and put them in the created folder.
6. Stick the docker folder into the windows %PATH% variable so that they can be seen from shell.
7. Register the docker daemon as a service. dockerd --register-service
8. Start the newly registered service Start-Service docker
9. Check the service si running. Get-Service docker
10. Run docker version and notice that both the client and server are running natively on the windows kernel
11. Before running containers you will need the base OS image for docker for windows server. Install-PackageProvider ContainerImage -Force
12. Run Find-ContainerImage to ensure that its been installed on the machine.
13. Install the container image that was just downloaded. Install-ContainerImage WindowsServerCore.
14. Restart the docker service when the image has finally been downloaded. Restart-Service docker
15. By typing docker images we should be able to see the Windowsservercore image installed.
16. Tag the new image as the latest version. docker tag <ImageID> <Imagename>:<tag>
docker tag 5bc36a3335435 windowsservercore:latest
17. Check docker images to see the images installed and you should see that there is two images now both based off the same imageID but one is now tagged correctly.
18. Start a new container using the latest image. docker run -it windowsservercore cmd (NOTE: if we didnt tag the image as latest we would have to specific the default version tag)

Installing docker in Linux:
The following will be using Ubuntu change package managers if necessary

1. Grab the install script and run it though your distro shell. wget -qO- https://get.docker.com/ | sh
2. If you want to use docker without the root account add a user to the docker group. usermod -aG docker <username>
3. Run docker version and notice that both the client and the server are running on linux.

##### Summary:

* Installing docker on each common flavour of OS

### C2: Working with Containers.

In the past hypervisor and other virtulisation tools. Slice up Physical resources such as CPU and drive space and split them into VMS which replicate the OS

Containers work differently. By slicing up OS components rather then Physical components each containers gets a PID, a Root File share and Networking interfaces.
This means they are more light weight and have less clutter then traditional VMS.

The docker engine is the standard install and it gives you the client and the daemon on the same host.

Useful docker commands:

* docker images > Check all the images on the local engine
* docker version > Check the current docker server and client version
* docker info > A bunch of information about the current docker host
* docker run hello-world > Run the container based on the hello-workd image.
* docker ps > Checks current containers running
* docker ps -a > Checks containers that have run but exited
* docker pull alpine > Pull the latest image under the name alpine from docker registry
* docker pull ubuntu:14.04 > Pull version 14.04 of the ubuntu image from the docker registry
* docker rmi ubuntu:14.04 > Removes the image from the local docker image store based on either image ID or name and tag
* docker stop relaxed_jolitot > stops the container with the relaxed_jolitot name
* docker start relaxed_jolitot > starts the container with the relaxed_jolitot name
* docker rm relaxed_jolitot > removes the container thus removing the data, with the relaxdd_jolitot name.
* docker rm $(docker ps -aq) > remove all containers on the local host

An example of a standard docker run command:
`docker run -d --name web -p 80:8080 nigelpoulton/pluralsight-docker-ci`

to break this command down.

docker > Calls the docker binary in the shell
run > Tells the daemon we are going to run a container
-d > Tells the container to run in detached mode so it doesnt stick to the terminal. (similar to nohup)
--name web > tells the daemon to run the container under the web name rather then using a randomly generated name such as relaxed_jolitot
-p 80:8080 > Maps the container 8080 port to the port 80 on the docker host e.g. in our brower we hit localhost:80 and it hits the 8080 in the container.
nigelpoulton/pluralsight-docker-ci > tells the daemon what image we are going to run.


Another useful example:
`docker run -it --name temp ubuntu:latest /bin/bash`

to break this command down.

docker > Calls the docker binary in the shell
run > Tells the daemon we are going to run a container
 -i > Keep STDIN open even if not attached
 -t > Allocate a pseudo-tty > emulate a text editor
 --name temp > allocate a container name
 ubuntu:latest > the current image we are using
 /bin/bash > the command we run on container start up > -it required for commands 

This will put you in a container only running bash. By typing exit you close the only service that the bash container is running so to exit press cmd+p+q to leave without closing bash

Here is how the docker run command works:
Client1: docker run hello-world sends an api call to the daemon
Daemon: Daemon implements the docker remote API
Daemon: Daemon checks local store for the hello-world image.
Daemon: If it doesnt have it. It will search docker hub for new registry. You can set the daemon to report to other registrys.
Daemon: Once image is pulled or the daemon has it stored in the docker local image store. the daemon then kicks up the container.

Images - Stopped containers
Containers - Running images

Here is how the docker pull command works:

1. Client1: docker pull alpine:3.55
2. Daemon: Daemon implements the docker remote API
3. Daemon: Daemon pulls the image and check if its in the local docker image store
4. DaemonL Daemon either downloads the image or reports if the image is already stored.

Here is how the docker images command works:

1. Client1: docker images
2. Daemon: Daemon implements the docker remote API
3. Daemon: Daemon prints the local image store the console and shows repository, tag, timestamp, size and other information

Here is how the docker rmi command works:

1. Client1: docker rmi 415314131
2. Daemon: Daemon implements the docker remote API
3. Daemon: Searches the local image store for the image id provided by the client
4. Daemon: Removes the image from the local docker image store.

Persisting data across containers is fairly easy. The data will only be removed from the container if its destroyed

##### Summary:
docker run is the command needed to spin up a container
Images are templates / not running containers
docker run is a client command that makes api calls to the daemon which does the heavy lifting.
The daemon implments the docker remote api
When creating a container the daemon searches the local imaege store first before checking the registry
You can run your containers detached -d
You can also interact with them by attaching a terminal to the STDIN stream -it

### C3: Swarm mode and microservices

docker 1.12 or higher needed.

A docker swarm consists of multiple docker engine nodes.

The swarm itself consists of 1 or more manager nodes and 1 or more worker nodes. The manager node are HA and distribute work to the worker nodes.

An odd number of nodes are recommended for manager nodes 3-5 similar to Consul. Only one of the manager nodes is elected a leader and if it goes down the failover goes down to another manager node.

Worker nodes simply accept tasks from managers and execute them. They are also incharge of setting up the docker services. 

Services from a docker perspective are based on the architechture of the application that is being contained. e.g. Web FrontEnd, Db backend

Note: Swarm is required for services.

`docker service create -- name web-fe --replicas 5`

docker > calls the docker binary in the shell
service > instructs the swarm to create a servi e
--name web-fe > gives the service the name web-fe
--replicas 5 > Instructs the swarm to bring up 5 instances of this service.

the worker nodes will now go and spin up 5 "tasks" If one of these tasks/containers goes down. The workers will realise and spin up another one.

A task is a unit of work assigned to a worker node.

You can think of a task as a container with a few extra pieces of metadata.

Building a swarm of 3 manager nodes and 3 worker nodes.

The swarm folder contains terraform scripts to bring up 6 nodes with docker pre installed. Follow the steps manually for now to understand how to set up a docker swarm.

On the first manager dtswarmm-0

`docker swarm init --advertise-addr 172.31.0.161:2377 --listen-addr 172.31.0.161:2377`
OR
`docker swarm init --advertise-addr eth0:2377 --listen-addr eth0:2377`

its possible to assign up or interface address as the above example indicates

docker > calls docker binary
swarm > tells the damon we are going to manage a swarm
init > intialise a swarm
--advertise-addr > the specific address that will be advertised to other nodes
--listen-addr > the address for this node to listen to inbound swarm traffic

for the initial manager we keep these the same incase a leader is elected elsewhere. 2377 is default swarm port.

If ran correctly you will get an output token

Swarm initialized: current node (ktatnwp03upkv3fwsvw4zwod6) is now a manager.

To add a worker to this swarm, run the following command:

`docker swarm join --token SWMTKN-1-2ioiapsdx7wppkn72mb247pfn3aiklre9dxbghz3f8qs0541cx-apa7fz89ufn7gef06jhkmvbj3 172.31.6.212:2377`

You can double check the token required by typing the following commands

`docker swarm join-token manager` > prints the token to join another manager to the swarm
`docker swarm joint-token worker` > prints the token to join another worker to the swarm

`docker info` will now show the swarm is active with one node. The manager nodes also act as worker nodes. so it should display the following.

Swarm: active
 NodeID: ktatnwp03upkv3fwsvw4zwod6
 Is Manager: true
 ClusterID: 5p49ksoeseelsd40rf4zgoub2
 Managers: 1
 Nodes: 1

On the second manager dtsawrmm-1

`docker swarm join --token SWMTKN-1-1bbl97t0pu9fu64mj72wjudnxhyjgxzp55zb6wmsjrgrhhh2ks-7uywlosa85l0sx5i8eczyvswt 172.31.4.205:2377 --advertise-addr eth0:2377 --listen-addr eth0:2377`

This node joined a swarm as a manager.

`docker info`

Swarm: active
 NodeID: 1wuq04pu2edu18x2hv6pgx5nb
 Is Manager: true
 ClusterID: 9yaz4ohhzl2h8oeh5zfft759g
 Managers: 2
 Nodes: 2

 Notice there is now 2 managers and 2 ndoes.

 docker node ls

 You can only run docker node ls from a manager

 ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS
1wuq04pu2edu18x2hv6pgx5nb *   ip-172-31-8-49      Ready               Active              Reachable
laruu05pgp4zcpbsj3koanqq0     ip-172-31-4-205     Ready               Active              Leader

Notice the asterisk shows us which machine we ran this command on.

on manager 3 we are going to set up a worker and promote it to manager.

`docker swarm join --token SWMTKN-1-1bbl97t0pu9fu64mj72wjudnxhyjgxzp55zb6wmsjrgrhhh2ks-ejowig2fnv0h2t2r97f5hw325 172.31.8.49:2377 --advertise-addr eth0:2377 --listen-addr eth0:2377`

when running docker node ls from our 2nd manager

ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS
1wuq04pu2edu18x2hv6pgx5nb *   ip-172-31-8-49      Ready               Active              Reachable
laruu05pgp4zcpbsj3koanqq0     ip-172-31-4-205     Ready               Active              Leader
ynqi7o4ml0ej82s3ypbpkpb7s     ip-172-31-6-186     Ready               Active

we can then run `docker node promote ynqi7o4ml0ej82s3ypbpkpb7s` 

ec2-user@ip-172-31-8-49 ~]$ docker node promote ynqi7o4ml0ej82s3ypbpkpb7s
Node ynqi7o4ml0ej82s3ypbpkpb7s promoted to a manager in the swarm.
[ec2-user@ip-172-31-8-49 ~]$ docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS
1wuq04pu2edu18x2hv6pgx5nb *   ip-172-31-8-49      Ready               Active              Reachable
laruu05pgp4zcpbsj3koanqq0     ip-172-31-4-205     Ready               Active              Leader
ynqi7o4ml0ej82s3ypbpkpb7s     ip-172-31-6-186     Ready               Active              Reachable

You can then join the other three workers using the worker join command

[ec2-user@ip-172-31-8-49 ~]$ docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS
1wuq04pu2edu18x2hv6pgx5nb *   ip-172-31-8-49      Ready               Active              Reachable
7w75e18n7w5ki5pvmj8rum9xo     ip-172-31-0-237     Ready               Active
car1db7lq4cnx4vnskcz24oin     ip-172-31-13-101    Ready               Active
htl7es6r1uylf6x27ae27v64g     ip-172-31-14-192    Ready               Active
laruu05pgp4zcpbsj3koanqq0     ip-172-31-4-205     Ready               Active              Leader
ynqi7o4ml0ej82s3ypbpkpb7s     ip-172-31-6-186     Ready               Active              Reachable

Note that if a command is issued on a manager its gets proxied to the leader by default. If the leader goes down a new one is automatically selected based on the raft protocol.

Creating a docker Service:

`docker service create --name psight1 -p 8080:8080 --replicas 5 nigelpoulton/pluralsight-docker-ci`

service > tells the daemon we are going to be creating a service.
--name psight1 > Gives the service the name psght1
-p 8080:8080> maps the port 8080 inside the replica/container/task to the swarm 80080
--replicas 5 > tells the swarm that we want 5 instances of the service
nigelpilton/pluralsight-docker-ci > states what image the service will be using

[ec2-user@ip-172-31-8-49 ~]$ docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE                                       PORTS
vaio9pal6bp5        psight1             replicated          0/5                 nigelpoulton/pluralsight-docker-ci:latest   *:8080->8080/tcp
[ec2-user@ip-172-31-8-49 ~]$ docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE                                       PORTS
vaio9pal6bp5        psight1             replicated          5/5                 nigelpoulton/pluralsight-docker-ci:latest   *:8080->8080/tcp

docker service ls shows the state of the service. It takes some time to install the images onto each worker node so they dont spin up instantly.

[ec2-user@ip-172-31-8-49 ~]$ docker service ps psight1
ID                  NAME                IMAGE                                       NODE                DESIRED STATE       CURRENT STATE           ERROR               PORTS
qjcuv5m0dzrh        psight1.1           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-6-186     Running             Running 2 minutes ago
iu71i7ltdgxn        psight1.2           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-0-237     Running             Running 2 minutes ago
cikzztpjps3a        psight1.3           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-14-192    Running             Running 2 minutes ago
i6qczlc9gp8y        psight1.4           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-13-101    Running             Running 2 minutes ago
joj8w70rvxoc        psight1.5           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-4-205     Running             Running 2 minutes ago

Lists each instance of the service with the name psight1

`docker service inspect psight1`

Provides JSON about the details of the current service created.

With the service now providing on 5 out of the 6 nodes we can still hit the last node on 8080 to get the site url. This inbuilt load balancing for docker is called routing mesh.

Scaling services:

When killing a node the swarm will automatically bring up another replica matching the desirted state. 

If we need more replicas we can increase the exsisting service

`docker service scale psight1=7`

[ec2-user@ip-172-31-8-49 ~]$ docker service scale psight1=7
psight1 scaled to 7

You can see the 2 replicas being brought up. Notice how the nodes can run multuple containers

[ec2-user@ip-172-31-8-49 ~]$ docker service ps psight1
ID                  NAME                IMAGE                                       NODE                DESIRED STATE       CURRENT STATE              ERROR               PORTS
qjcuv5m0dzrh        psight1.1           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-6-186     Running             Running 22 minutes ago
iu71i7ltdgxn        psight1.2           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-0-237     Running             Running 22 minutes ago
cikzztpjps3a        psight1.3           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-14-192    Running             Running 22 minutes ago
i6qczlc9gp8y        psight1.4           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-13-101    Running             Running 22 minutes ago
joj8w70rvxoc        psight1.5           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-4-205     Running             Running 22 minutes ago
16xw4pv5kug3        psight1.6           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-8-49      Running             Preparing 16 seconds ago
pahyglesn4s8        psight1.7           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-8-49      Running             Preparing 16 seconds ago

killing a node will make the swarm realise its down. When the node restarts the swarm realises and makes it available again

[ec2-user@ip-172-31-8-49 ~]$ docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS
1wuq04pu2edu18x2hv6pgx5nb *   ip-172-31-8-49      Ready               Active              Reachable
7w75e18n7w5ki5pvmj8rum9xo     ip-172-31-0-237     Down                Active
car1db7lq4cnx4vnskcz24oin     ip-172-31-13-101    Ready               Active
htl7es6r1uylf6x27ae27v64g     ip-172-31-14-192    Ready               Active
laruu05pgp4zcpbsj3koanqq0     ip-172-31-4-205     Ready               Active              Leader
ynqi7o4ml0ej82s3ypbpkpb7s     ip-172-31-6-186     Ready               Active              Reachable
[ec2-user@ip-172-31-8-49 ~]$ docker node ls
ID                            HOSTNAME            STATUS              AVAILABILITY        MANAGER STATUS
1wuq04pu2edu18x2hv6pgx5nb *   ip-172-31-8-49      Ready               Active              Reachable
7w75e18n7w5ki5pvmj8rum9xo     ip-172-31-0-237     Ready               Active
car1db7lq4cnx4vnskcz24oin     ip-172-31-13-101    Ready               Active
htl7es6r1uylf6x27ae27v64g     ip-172-31-14-192    Ready               Active
laruu05pgp4zcpbsj3koanqq0     ip-172-31-4-205     Ready               Active              Leader
ynqi7o4ml0ej82s3ypbpkpb7s     ip-172-31-6-186     Ready               Active              Reachable

The replicas will be automatically load balanced to the node most suitable in terms of resources to support it.

E.g. If we have 10 nodes in a swarm and deploy 20 service replicas. If each node has the same starting amount of resources then each node should be hosting 2 replicas.

If you add a new node to the swarm of 10 newly added nodes wont automatically rebalance exsisting running tasks.

To check a specific node workload

[ec2-user@ip-172-31-8-49 ~]$ docker node ps ip-172-31-6-186
ID                  NAME                IMAGE                                       NODE                DESIRED STATE       CURRENT STATE               ERROR               PORTS
qjcuv5m0dzrh        psight1.1           nigelpoulton/pluralsight-docker-ci:latest   ip-172-31-6-186     Running             Running about an hour ago

Rolling Updates:

Starting with a clean service we can use docker service rm psight1

[ec2-user@ip-172-31-8-49 ~]$ docker service rm psight1
psight1
[ec2-user@ip-172-31-8-49 ~]$ docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE               PORTS

First lets do the same as above but we will be creating an overlay network to make node communication easier.

`docker network create -d overlay ps-net`

`docker network ls`

NETWORK ID          NAME                DRIVER              SCOPE
507dd6e539c1        bridge              bridge              local
ca3ba3ceca3c        docker_gwbridge     bridge              local
38130894db14        host                host                local
uk3kieqhvubd        ingress             overlay             swarm
7a970bd3f886        none                null                local
wt587j9vw64k        ps-net              overlay             swarm

Then we will create another service on the ps-net network using a similar command as before

[ec2-user@ip-172-31-4-205 ~]$ docker service create --name psight2 --network ps-net -p 80:80 --replicas 12 nigelpoulton/tu-demo:v1
lsyy1di0z54hpi26y64rvt25r
Since --detach=false was not specified, tasks will be created in the background.
In a future release, --detach=false will become the default.
[ec2-user@ip-172-31-4-205 ~]$ docker service ls
ID                  NAME                MODE                REPLICAS            IMAGE                     PORTS
lsyy1di0z54h        psight2             replicated          0/12                nigelpoulton/tu-demo:v1   *:80->80/tcp

[ec2-user@ip-172-31-4-205 ~]$ docker service ps psight2
ID                  NAME                IMAGE                     NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
j9jolz1zplh7        psight2.1           nigelpoulton/tu-demo:v1   ip-172-31-0-237     Running             Running 31 seconds ago
nicqikwnb0bt        psight2.2           nigelpoulton/tu-demo:v1   ip-172-31-13-101    Running             Running 31 seconds ago
yhqbo9y9isiq        psight2.3           nigelpoulton/tu-demo:v1   ip-172-31-13-101    Running             Running 31 seconds ago
ys7yuacb21my        psight2.4           nigelpoulton/tu-demo:v1   ip-172-31-6-186     Running             Running 31 seconds ago
lafy05sa6388        psight2.5           nigelpoulton/tu-demo:v1   ip-172-31-6-186     Running             Running 31 seconds ago
urryguy70nau        psight2.6           nigelpoulton/tu-demo:v1   ip-172-31-14-192    Running             Running 31 seconds ago
kz2hfdel2ta9        psight2.7           nigelpoulton/tu-demo:v1   ip-172-31-8-49      Running             Running 31 seconds ago
gckf8pfz6k77        psight2.8           nigelpoulton/tu-demo:v1   ip-172-31-14-192    Running             Running 31 seconds ago
iq66vt5eilgv        psight2.9           nigelpoulton/tu-demo:v1   ip-172-31-4-205     Running             Running 31 seconds ago
b1dtd6907mh0        psight2.10          nigelpoulton/tu-demo:v1   ip-172-31-8-49      Running             Running 31 seconds ago
25zul0r25hwi        psight2.11          nigelpoulton/tu-demo:v1   ip-172-31-0-237     Running             Running 31 seconds ago
04o0bllti0sv        psight2.12          nigelpoulton/tu-demo:v1   ip-172-31-4-205     Running             Running 31 seconds ago

notice how each node has 2 instances because the replicas are automatically being load balanced correctly by the swarm.

If we hit the public ip of one of these nodes on port 80 we can see the voting app displayed.

also you will notice how they are all version 1 of the image matching the tag on the service create.

We can view the service by running the docker service inspect --pretty psight2 command to see our service configuration

[ec2-user@ip-172-31-4-205 ~]$ docker service inspect --pretty psight2

ID:		lsyy1di0z54hpi26y64rvt25r
Name:		psight2
Service Mode:	Replicated
 Replicas:	12
Placement:
UpdateConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Update order:      stop-first
RollbackConfig:
 Parallelism:	1
 On failure:	pause
 Monitoring Period: 5s
 Max failure ratio: 0
 Rollback order:    stop-first
ContainerSpec:
 Image:		nigelpoulton/tu-demo:v1@sha256:9ccc0c67e5c5eaae4bebeeed9b22e0e22f8a35624c1d5c80f2c9623cbcc9b59a
Resources:
Networks: ps-net
Endpoint Mode:	vip
Ports:
 PublishedPort = 80
  Protocol = tcp
  TargetPort = 80
  PublishMode = ingress


Notice that parrallism is set to 1 and update-delay is set 0. Parrallism is how many replicas it updates at a time with update delay being how long each update waits between each replicas. You can define these on service creation with flags or update them using.

`docker service update --image nigelpoulton/tu-demo:v2 --update-parallelism 2 --update-delay 10s`

This will bring the service up with the new image. Update the parrallsism to do 2 replicas at a time and then wait 10 seconds between each replicas.

[ec2-user@ip-172-31-4-205 ~]$ docker service ps psight2
ID                  NAME                IMAGE                     NODE                DESIRED STATE       CURRENT STATE            ERROR               PORTS
u5sdoa5y8usr        psight2.1           nigelpoulton/tu-demo:v2   ip-172-31-0-237     Ready               Ready 11 seconds ago
j9jolz1zplh7         \_ psight2.1       nigelpoulton/tu-demo:v1   ip-172-31-0-237     Shutdown            Running 13 seconds ago
qliqc58266j9        psight2.2           nigelpoulton/tu-demo:v2   ip-172-31-13-101    Ready               Ready 10 seconds ago
nicqikwnb0bt         \_ psight2.2       nigelpoulton/tu-demo:v1   ip-172-31-13-101    Shutdown            Running 13 seconds ago
yhqbo9y9isiq        psight2.3           nigelpoulton/tu-demo:v1   ip-172-31-13-101    Running             Running 12 minutes ago
ys7yuacb21my        psight2.4           nigelpoulton/tu-demo:v1   ip-172-31-6-186     Running             Running 12 minutes ago
lafy05sa6388        psight2.5           nigelpoulton/tu-demo:v1   ip-172-31-6-186     Running             Running 12 minutes ago
urryguy70nau        psight2.6           nigelpoulton/tu-demo:v1   ip-172-31-14-192    Running             Running 12 minutes ago
kz2hfdel2ta9        psight2.7           nigelpoulton/tu-demo:v1   ip-172-31-8-49      Running             Running 12 minutes ago
gckf8pfz6k77        psight2.8           nigelpoulton/tu-demo:v1   ip-172-31-14-192    Running             Running 12 minutes ago
iq66vt5eilgv        psight2.9           nigelpoulton/tu-demo:v1   ip-172-31-4-205     Running             Running 12 minutes ago
b1dtd6907mh0        psight2.10          nigelpoulton/tu-demo:v1   ip-172-31-8-49      Running             Running 12 minutes ago
25zul0r25hwi        psight2.11          nigelpoulton/tu-demo:v1   ip-172-31-0-237     Running             Running 12 minutes ago
04o0bllti0sv        psight2.12          nigelpoulton/tu-demo:v1   ip-172-31-4-205     Running             Running 12 minutes ago

Notice how the first two have already rolled and just kicked off as the command was run. Over the process fo the next two minutes each replicas will be updated. If we hit the app again we should see the new version. As each node is internally load balanced you might need to hit refresh each time till you hit a replica with the update installed.

if you run `docker service inspect --pretty psight2` you should also see our new updated delay and parralleism on the configuration

UpdateConfig:
 Parallelism:	2
 Delay:		10s

Stacks and bundles:##############################################################################################################################

Experimental builds only for now

Install the experimental build with the following:



each application consists of multiple parts such as a backend or MQ, frontend. To bundle all of these into once service we can use stacks and bundles.

First clone the application we are going to be creating the stack for

`git clone https://github.com/dockersamples/example-voting-app.git`

The architechture of this app consists of 5 services. The voting app(python), results app(node.js), redis, db, .net. All 5 of these microservices can be bundles and scaled independently into one main service.

Looking at the docker-compose.yml we can see the declarations of our services.

version: "3"

services:
  vote:
    build: ./vote
    command: python app.py
    volumes:
     - ./vote:/app
    ports:
      - "5000:80"
    networks:
      - front-tier
      - back-tier

  result:
    build: ./result
    command: nodemon server.js
    volumes:
      - ./result:/app
    ports:
      - "5001:80"
      - "5858:5858"
    networks:
      - front-tier
      - back-tier

  worker:
    build:
      context: ./worker
    depends_on:
      - "redis"
    networks:
      - back-tier

  redis:
    image: redis:alpine
    container_name: redis
    ports: ["6379"]
    networks:
      - back-tier

  db:
    image: postgres:9.4
    container_name: db
    volumes:
      - "db-data:/var/lib/postgresql/data"
    networks:
      - back-tier

volumes:
  db-data:

networks:
  front-tier:

Here you can see the version of compose we need. The services we have declared (vote, result, worker). And there associated requirements and build instructions.

For docker swarm to be able to use this file we need docker compose.

To install docker compose grab it from the following url.

`sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose`

Then make it executable

`sudo chmod +x /usr/local/bin/docker-compose`

[ec2-user@ip-172-31-4-205 example-voting-app]$ sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose
  % Total    % Received % Xferd  Average Speed   Time    Time     Time  Current
                                 Dload  Upload   Total   Spent    Left  Speed
100   617    0   617    0     0   1278      0 --:--:-- --:--:-- --:--:--  1280
100 8280k  100 8280k    0     0  1365k      0  0:00:06  0:00:06 --:--:-- 1844k
[ec2-user@ip-172-31-4-205 example-voting-app]$ sudo chmod +x /usr/local/bin/docker-compose
[ec2-user@ip-172-31-4-205 example-voting-app]$ docker-compose --version

We can now convert the compose file to a dab file (distributed application bundle) so it can be interpreted by swarm.

run docker-compose build in the directory of the docker-compose.yml file

Notice the context and build values in the docker-compose.yml file. For us to bundle this file into a dab file we need to convert them to use actual images rather then hard coded paths such as /app.py

to do this first take a look at docker images. to see what images were downloaded when running docker-compose

ec2-user@ip-172-31-4-205 example-voting-app]$ docker images
REPOSITORY                           TAG                 IMAGE ID            CREATED             SIZE
examplevotingapp_result              latest              77efb7e8e4e3        2 minutes ago       81.7MB
examplevotingapp_worker              latest              e0f27c8aa9f7        2 minutes ago       1.72GB
examplevotingapp_vote                latest              01265d0ff0a0        3 minutes ago       83.9MB
python                               2.7-alpine          0781c116c406        32 hours ago        72.4MB
node                                 8.9-alpine          7c2983dfbf98        37 hours ago        68.1MB
microsoft/dotnet                     2.0.0-sdk           fde8197d13f4        3 months ago        1.64GB
nigelpoulton/tu-demo                 <none>              6ac21e29bead        20 months ago       212MB
nigelpoulton/tu-demo                 <none>              9b915a241e29        20 months ago       212MB
nigelpoulton/pluralsight-docker-ci   <none>              07e574331ce3        2 years ago         557MB

The ones that were created most recently are the ones that need tagging in the docker-compose.yml as they were built locally. Lets tag the top 3 so we can push them to a docker hub repo and call them from there rather then source them locally as thats currently not working.

`docker tag examplevotingapp_result pudd1ng/examplevotingapp_result`

`docker tag examplevotingapp_worker pudd1ng/examplevotingapp_worker`

`docker tag examplevotingapp_vote pudd1ng/examplevotingapp_vote`


Once we have tagged these we can login and push them.

`docker login`

and then push them.

`docker push pudd1ng/examplevotingapp_result`

[ec2-user@ip-172-31-4-205 example-voting-app]$ docker tag examplevotingapp_result pudd1ng/examplevotingapp_result
[ec2-user@ip-172-31-4-205 example-voting-app]$
[ec2-user@ip-172-31-4-205 example-voting-app]$ docker tag examplevotingapp_worker pudd1ng/examplevotingapp_worker
[ec2-user@ip-172-31-4-205 example-voting-app]$ docker tag examplevotingapp_vote pudd1ng/examplevotingapp_vote
[ec2-user@ip-172-31-4-205 example-voting-app]$ docker login
Login with your Docker ID to push and pull images from Docker Hub. If you don't have a Docker ID, head over to https://hub.docker.com to create one.
Username: pudd1ng
Password:
Login Succeeded
[ec2-user@ip-172-31-4-205 example-voting-app]$
[ec2-user@ip-172-31-4-205 example-voting-app]$ docker push pudd1ng/examplevotingapp_result
The push refers to a repository [docker.io/pudd1ng/examplevotingapp_result]
233476737de5: Pushed
d0041cc58d48: Pushed
e9b36ca7741b: Pushed
a04f02fd581e: Pushed
f5951e7e6aa8: Pushed
e15cbf38e481: Pushed
f3d6daebf694: Mounted from library/node
ef6ea5eda60b: Mounted from library/node
9dfa40a0da3b: Mounted from library/node
latest: digest: sha256:d5e860a32a4ff1814b41e8249ad2dc9373a05361af3821a359672ab2f54ebd3b size: 2202


Push every image with the push command.

Now lets format the compose file so that dab can work with it by removing the volume lines and replacing the build lines with image lines. We also remove the networking lines as they are not compatible at the moment either.

version: "3"

services:
  vote:
    image: pudd1ng/examplevotingapp_vote:latest
    command: python app.py
    ports:
      - "5000:80"

  result:
    image: pudd1ng/examplevotingapp_result:latest
    command: nodemon server.js
    ports:
      - "5001:80"
      - "5858:5858"

  worker:
    image: pudd1ng/examplevotingapp_worker:latest
    depends_on:
      - "redis"

  redis:
    image: redis:alpine
    container_name: redis
    ports: ["6379"]

  db:
    image: postgres:9.4
    container_name: db

Here is an example of a working docker-compose.yml

run docker-compose bundle
 Notice how we need to pull the redis image still. Run docker-compose pull redis


ec2-user@ip-172-31-4-205 example-voting-app]$ docker-compose bundle
ERROR: Image not found for service 'redis'. You might need to run `docker-compose pull redis`.
[ec2-user@ip-172-31-4-205 example-voting-app]$ docker-compose pull redis
Pulling redis (redis:alpine)...
alpine: Pulling from library/redis
605ce1bd3f31: Already exists
d12d456b03b7: Pull complete
b6e6ee69c5e2: Pull complete
813ea0248ee4: Pull complete
62208b7f07e1: Pull complete
35b7abefa067: Pull complete
Digest: sha256:a5fd9334b28d6afdc8b84aad8a01056d8d1d849cb5a744919caf40d5b57597b2
Status: Downloaded newer image for redis:alpine

We might need to pull some more images so run `docker-compose pull db`

[ec2-user@ip-172-31-4-205 example-voting-app]$ docker-compose bundle
WARNING: Unsupported key 'depends_on' in services.worker - ignoring
WARNING: Unsupported key 'container_name' in services.db - ignoring
WARNING: Unsupported key 'container_name' in services.redis - ignoring
Wrote bundle to examplevotingapp.dab

Finally a dab file. This is still in developmenbt so some keys are ignored and not supported.

now that we have the dab file we can now deploy it

`docker stack deploy examplevotingapp --bundle-file examplevotingapp.dab`

`docker service ls` will now show you the new app and if you hit the port defined in the compose file you should be able to hit that on your nodes public ip.

##### Summary

Spinning up a swarm, adding nodes and wokers
Creating services to scale and monitor groups of containers running the same image.
Learnt how to update services and there configurations
Creating Stacks to scale and monitor a bundle of services of containers across the swarm.


* docker swarm join
* docker service create
* docker service scale
* docker service update
* docker-compose
* docker stack deploy