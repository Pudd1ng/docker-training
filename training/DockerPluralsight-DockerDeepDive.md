# Installing Docker

Refer to Getting Started with Docker for in-depth install guides.

https://labs.play-with-docker.com

If you want to play around without installing.

### Architechture and Theory
*  Picture View
* Kernel Internals
* Docker Engine
* Recap

##### The Big Picture 

Container : Isolated area of an OS with resource usage limits applied.

Consists of namespaces and control groups of the kernel. The docker engine makes using these building blocks and producing a working container easy.

Namespaces let us take an operating system and let us carve into multiple isolated versions of the OS

Similar to how hypervisor gives each VM its own RAM, CPU, Storage taken from the Physical machine

##### Kernel Internals

Container runtimes such as docker give containers there own file directories, process trees, eth0 interface and root user from the OS
A docker container consists of the following namespaces of an OS.

* PROCESSID(PID) - Each container has its own process id tree unique to that container
* NETWORK(NET) - Each container has its own isolated network stack (iptabeles ,interfaces)
* FILESYSTEM(MNT) - Each container has its own root filesystem (/ ,c:/)
* INTER-PROC-COMMS(IPC) - Each container has its own IPC letting the containers process access the same shared memory
* UTS(UTS) - Each container has its own hostname
* USER(USER) - Gives each container its own user account mapping.

The concept is the same for windows. but with the windows terminology. Object instead of PID etc.

Control groups group processes and impose limits on each container. e.g. Container A only gets a certain amount of cpu, ram and disk Where as B could have more. The point being a limit is hard defined so one container cant eat everything.

##### The Docker Engine

The docker Engine works in the following way. 

###### Linux

1. Docker client takes the inputted command and relays it to the damon via rest 
2. The daemon then relays commands to the containerd runtime which manages containers
3. runc orchestrated by containerd then builds the containers themselves.

###### Windows

1. Docker client takes the inputted command and relays it to the damon via rest
2. The damon then relays commands to the conatinerd runetime which manages containers
3. Compute services takes over containerd and runc work

##### Low Level container creation example 

            Client : docker container run pudd1ng/web1:latest <-- User input
                                    |
                                    v
            {REST} POST /v1x.x/containers/create HTTP/1.1 <-- call to Daemon
                                    |
                                    v
            Daemon 
            {GRPC} client.NewConatiner(param1) <-- call to containerd
                                    |
                                    v
                        containerd <-- runtime
                              shim process
                                    |
                                    v
                       runc <-- container creation
                                    |
                                    v
                                CONTAINER

##### Recap 

Major kernel technologies = namespaces and cgroups
Namespaces let us isolate areas of an os into a container.
Cgroups let us cap the containers resource usage.
Layered filesystems allow each containers own OS shared off the Physical machines kernel

The engine is modular with the Client - Daemon - Runtime - library archtechture

* Client = docker client
* Daemon = docker daemon
* containerd = manage runtime
* runc = build runtime

##### Docker Images

###### Image overview 

A image is a read only template for a container. They are read only and stored in a registry. Under the covers an image is JSON formatted files and a manifest.

They need to be pulled from a registry onto a local machine to spin up containers on your host.

As the images are ready only when spinning up a conatiner a writable layer is overlapped and attached to that specific container. Any writing the container needs to do to fufill the image is done here.

##### Images in detail **

To get an image you need to pull it from a registry as mentioned before. To do this you can use the docker image pull command.

    ✘-1 ~/Projects/docker-training [master ↓·1↑·1|✚ 1…3]
    12:41 $ docker image pull redis
    Using default tag: latest
    latest: Pulling from library/redis
    c4bb02b17bb4: Pull complete
    58638acf67c5: Pull complete
    f98d108cc38b: Pull complete
    83be14fccb07: Pull complete
    5d5f41793421: Pull complete
    ed89ff0d9eb2: Pull complete
    Digest: sha256:0e773022cd6572a5153e5013afced0f7191652d3cdf9b1c6785eb13f6b2974b1
    Status: Downloaded newer image for redis:latest

Each pull complete shown above is a layer of the image. 

The image itself is a group of independant layers grouped together by a manifest file. Which acts as configuration for the image.

It includes the master list of the layers and how to stack them. 

When running docker image pull a number of steps is taken by the daemon once it recieves the api call.

1. The fat manifest or manifest list is grabbed from the repo. This manifest contains all the images for each architechture(x86/arm).
1.5 Once the image is found in the fat manifest its pulled down as an image manifest onto the local machine.
2. The image manfiest pulls all layers of said image onto the local machine.

Docker uses sha256 to gurantee what we pull from the registry is what is actually requested.

Each layer represents a piece of the over all container. Usually it would start with a base layer which is mainly a default OS such as ubuntu. This would contain the root ubuntu filesyste,

The second layer could be application specific such as application code. This would contain the code and build steps needed to create the app.

The third layer could be updates or 

Once all these layers are joined. The docker storage driver(overlay2) links them all up according to the manifest file pulled from the registry

Its possible to inspect the pulled image by running docker image inspect

    12:56 $ docker image inspect redis
    [
        {
            "Id": "sha256:1e70071f4af45af2cc9e1d1300c675c1ce37ee25a8a5cef1f375db5ed461dbab",
            "RepoTags": [
                "redis:latest"
            ],
            "RepoDigests": [
                "redis@sha256:0e773022cd6572a5153e5013afced0f7191652d3cdf9b1c6785eb13f6b2974b1"
            ],
            "Parent": "",
            "Comment": "",
            "Created": "2017-12-12T07:15:58.901779813Z",
            "Container": "cf72799c32e80417751c6657f680d42cef1f5a03befe10074860e9fdfa8d9709",
            "ContainerConfig": {
                "Hostname": "cf72799c32e8",
                "Domainname": "",
                "User": "",
                "AttachStdin": false,
                "AttachStdout": false,
                "AttachStderr": false,
                "ExposedPorts": {
                    "6379/tcp": {}
                },
                "Tty": false,
                "OpenStdin": false,
                "StdinOnce": false,
                "Env": [
                    "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                    "GOSU_VERSION=1.10",
                    "REDIS_VERSION=4.0.6",
                    "REDIS_DOWNLOAD_URL=http://download.redis.io/releases/redis-4.0.6.tar.gz",
                    "REDIS_DOWNLOAD_SHA=769b5d69ec237c3e0481a262ff5306ce30db9b5c8ceb14d1023491ca7be5f6fa"
                ],
                "Cmd": [
                    "/bin/sh",
                    "-c",
                    "#(nop) ",
                    "CMD [\"redis-server\"]"
                ],
                "ArgsEscaped": true,
                "Image": "sha256:03e433d6dc5431c878f412f8a65064556876c6dbad3393bd2d9cfff6ee66b794",
                "Volumes": {
                    "/data": {}
                },
                "WorkingDir": "/data",
                "Entrypoint": [
                    "docker-entrypoint.sh"
                ],
                "OnBuild": [],
                "Labels": {}
            },
            "DockerVersion": "17.06.2-ce",
            "Author": "",
            "Config": {
                "Hostname": "",
                "Domainname": "",
                "User": "",
                "AttachStdin": false,
                "AttachStdout": false,
                "AttachStderr": false,
                "ExposedPorts": {
                    "6379/tcp": {}
                },
                "Tty": false,
                "OpenStdin": false,
                "StdinOnce": false,
                "Env": [
                    "PATH=/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin",
                    "GOSU_VERSION=1.10",
                    "REDIS_VERSION=4.0.6",
                    "REDIS_DOWNLOAD_URL=http://download.redis.io/releases/redis-4.0.6.tar.gz",
                    "REDIS_DOWNLOAD_SHA=769b5d69ec237c3e0481a262ff5306ce30db9b5c8ceb14d1023491ca7be5f6fa"
                ],
                "Cmd": [
                    "redis-server"
                ],
                "ArgsEscaped": true,
                "Image": "sha256:03e433d6dc5431c878f412f8a65064556876c6dbad3393bd2d9cfff6ee66b794",
                "Volumes": {
                    "/data": {}
                },
                "WorkingDir": "/data",
                "Entrypoint": [
                    "docker-entrypoint.sh"
                ],
                "OnBuild": [],
                "Labels": null
            },
            "Architecture": "amd64",
            "Os": "linux",
            "Size": 106673855,
            "VirtualSize": 106673855,
            "GraphDriver": {
                "Data": {
                    "LowerDir": "/var/lib/docker/overlay2/c6b81ac20dce33d73287517cc304ace890077671ac333de812f2e197f57cc52e/diff:/var/lib/docker/overlay2/380f6e787d98b337f8bd99a879882c594f05820769b6c8d7b750d3281ad9857c/diff:/var/lib/docker/overlay2/ff365725a2ab0cfb8cc66343b22e9f41317d303e2060a513ad4375782c3bf677/diff:/var/lib/docker/overlay2/1ec326767c107d762b7e0fe4d53b36add4842c8b87d096cb1c552eacff0ac828/diff:/var/lib/docker/overlay2/3fd7e02cc5ebc6fb3b5fa44c2268740b4f05434e8f485193c9c64227fb722b35/diff",
                    "MergedDir": "/var/lib/docker/overlay2/f218e134f6da21b648d5576f1bd9e3ba38579ea7046ad3d99e8acda88902afec/merged",
                    "UpperDir": "/var/lib/docker/overlay2/f218e134f6da21b648d5576f1bd9e3ba38579ea7046ad3d99e8acda88902afec/diff",
                    "WorkDir": "/var/lib/docker/overlay2/f218e134f6da21b648d5576f1bd9e3ba38579ea7046ad3d99e8acda88902afec/work"
                },
                "Name": "overlay2"
            },
            "RootFS": {
                "Type": "layers",
                "Layers": [
                    "sha256:cfce7a8ae6322bbbd827e1d7b401abbc81ab1663fd20b2037895cc3eff2aec6f",
                    "sha256:bf0b6dc2d2d784ee1da4ab9b62d8f058a8814cf2a781f768ebb8e6cd72514127",
                    "sha256:2ea66b75bc3277544739f36527e32b99896e81ec43bbc5e6fd7b4267dc92a5a0",
                    "sha256:215555938ee1264db44d3a0773e95e66d9c1337b8a5a956dc1f93976896a6f38",
                    "sha256:031c196d6ffa60ff1d9e8443757b524543ee15590c29035c8438102a798c1b41",
                    "sha256:f37e5c6533609d1f650aac59fed03ae11157d9f1cd22e630821e892b88c84f6a"
                ]
            }
        }
    ]


here we can see the meta data and hashes of the redis image we downloaded earlier.

if we want to remove the image we can run docker image rm redis.

    12:57 $ docker image rm redis
    Untagged: redis:latest
    Untagged: redis@sha256:0e773022cd6572a5153e5013afced0f7191652d3cdf9b1c6785eb13f6b2974b1
    Deleted: sha256:1e70071f4af45af2cc9e1d1300c675c1ce37ee25a8a5cef1f375db5ed461dbab
    Deleted: sha256:a46ad6caee71cc51ba9a00be08d9555a0c36a3dcf937bae74149407b0deb3688
    Deleted: sha256:3474b667d8fa6f0e929cff2d53074bb498b7f4053998e016bc94f6bb88c4b48c
    Deleted: sha256:2e8aef0e625e6549279842f542cc8f010d73e979c9ebf1ca3d8fbf6f82677cda
    Deleted: sha256:de1ba52921504e1cc15e48069fcdad54eb61fde74d2e59bb353e3d80cab88a3f
    Deleted: sha256:fe31472d9b6ed188f80228e424bb2bd4a65a36cc90438dcaf6c873c8b50f66d2
    Deleted: sha256:cfce7a8ae6322bbbd827e1d7b401abbc81ab1663fd20b2037895cc3eff2aec6f

If you want to find the local image store its under /var/lib/docker/(storage driver)


#### Docker registrys 

When pulling an image to a host docker automatically uses the docker hub public registry. But if a private registry is needed you can use DTR or docker trusted registry.

When pulling an image it goes into the directory mentioned above.

A full docker image pull command without the shortcuts frmo the cli. would like look like the following

docker image pull docker.io/redis:latest

docker image pull registry/repo:image

We can get away with docker image pull redis because of the defaults. If you are not explicit about the registry it assumes docker hub(docker.io) and if you are not explicit about the image it assumes latests version

docker image pull docker.io/redis:latest

    ✔ ~/Projects/docker-training [master|✔]
    13:35 $ docker image pull docker.io/redis:latest
    latest: Pulling from library/redis
    c4bb02b17bb4: Pull complete
    58638acf67c5: Pull complete
    f98d108cc38b: Pull complete
    83be14fccb07: Pull complete
    5d5f41793421: Pull complete
    ed89ff0d9eb2: Pull complete
    Digest: sha256:0e773022cd6572a5153e5013afced0f7191652d3cdf9b1c6785eb13f6b2974b1
    Status: Downloaded newer image for redis:latest
    ✔ ~/Projects/docker-training [master|✔]
    13:35 $ docker image pull redis
    Using default tag: latest
    latest: Pulling from library/redis
    Digest: sha256:0e773022cd6572a5153e5013afced0f7191652d3cdf9b1c6785eb13f6b2974b1
    Status: Image is up to date for redis:latest

Notice how it hash is the same on both commands so its not redownloaded the same image.

docker image ls --digests can show the hash for each image currently installed on 

Each layer has a content hash to verify its contents. When compressed and sent over to the registry on upload new "distribution hashes" are formed as compression changes the content of the hash. These distributuon hashes are also added to the manifest to ensure the registry that what is being pushed is correct.

Uncompressed layers on the host - Content Hash
Pushing and pulling from registry - Distribution Hash
Uid for file system - Random

##### Best Practices with images 

1. Where possible used offical images.
2. Keep your images small.
3. Build on official images if they dont contain what you need.
4. Be explicit when downloading images. Latest is not always what its tagged to be.


##### Recap 

An image is a template for starting containers. It's read only and is built by stacking layers with the storage driver to make it look like a file system.

It consists of a config file and seperate layers to create the over all image. The config file has the instruction on how to run it as the container.

It's possible to start multiple containers per image and each container has its own writable layer to store its changed. No data is ever written to the image. 

When downloading an image from the registry its stored in /var/lib/docker/storagedriver

each layer have there own content hashes to ensure what you pull locally is correct.

compressed images when pushed have there own distribtuion hashes to ensure registy content is secure.

Offical repos are the safest and follow best practices.

docker image push > push to a registry
docker image pull > pull from a registry
docker image inspect > inspect image meta data and hashes
docker image rm > remove the image.

### Containerizing an app

Putting an application into an image so we can run a container from it.

** Containerizing the app **

To containerize aplication code you first need a Dockerfile. It's case sensitive so. Its also best practice to put this file in the root folder of the application.

touch Dockerfile

The dockerfile will contain the instructions for building the image. 

First things first in the Dockerfile you will needa base image to start from. For this example I will use alpine

FROM ubuntu:14.04

capitlized words are always instructions. The dockerfile should always start with FROM which is the initial layer.

next we will add a label to give the dockerfile some human readable instructions

LABEL maintainer="conner@ecs-digital.co.uk"

Next step is to add some software to the image. The RUN command executes the command and creates a layer.

RUN apt-get install ruby

Now that we have our software installed we need to add the application content. We can do this with the copy command. COPY like RUN creates another layer. Below we say copy everything from the current directory to the /app folder of the image.

COPY . /app

We then set the working DIR to /app this does not create another later

WORKDIR /src/app

WE then install GEMS using RUN so we get another layer

EXPOSE 4657

RUN bundle install

ENTRYPOINT ruby ./app.rb

Here is the full dockerfile

FROM ubuntu:14.04
LABEL maintainer="conner@ecs-digital.co.uk"
RUN apt-get install ruby
COPY . /app
WORKDIR /src/app
RUN bundle install
EXPOSE 4657

If we want to build it 
docker build -t hc2 -f Dockerfile .
-t tag -f Docker file location 
. = current content

you can now run containers from this image docker container run hc2

** digging deeper **

