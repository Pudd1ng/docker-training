variable "ami_id" {
  type = "string"
  description = "ami_id of the instance"
}
variable "number_of_instances" {
  type = "string"
  description = "number of instances to spin up"
}
variable "vpc_id" {
  type = "string"
  description = "id of the vpc"
}
variable "subnet_id" {
  type = "string"
  description = "the id of the subnet to use"
}
variable "instance_type" {
  type = "string"
  description = "the size of the instance needed"
}
variable "user_data" {
  type = "string"
  description = "path to the docker install script"
}
variable "keyname" {
  type = "string"
  description = "name of the keypair to use"
}
variable "instance_name_m" {
  type = "string"
  description = "name of the manager instances"
}
variable "instance_name_w" {
  type = "string"
  description = "name of the worker instances"
}