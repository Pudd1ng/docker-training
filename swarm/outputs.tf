output "instance_id_manager" {
  value = ["${aws_instance.manager_instance.*.id}"]
}

output "instance_ip_manager" {
  value = ["${aws_instance.manager_instance.*.public_ip}"]
}
output "instance_id_worker" {
  value = ["${aws_instance.worker_instance.*.id}"]
}

output "instance_ip_worker" {
  value = ["${aws_instance.worker_instance.*.public_ip}"]
}
