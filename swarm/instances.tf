resource "aws_instance" "manager_instance" {
  ami                    = "${var.ami_id}"
  count                  = "${var.number_of_instances}"
  subnet_id              = "${var.subnet_id}"
  instance_type          = "${var.instance_type}"
  user_data              = "${file(var.user_data)}"
  key_name               = "${var.keyname}"
  vpc_security_group_ids = ["${aws_security_group.dockerswarm.*.id}"]
  tags {
    Name   = "${var.instance_name_m}-${count.index}"
    Swarm = "Manager"
  }
}

resource "aws_instance" "worker_instance" {
  ami                    = "${var.ami_id}"
  count                  = "${var.number_of_instances}"
  subnet_id              = "${var.subnet_id}"
  instance_type          = "${var.instance_type}"
  user_data              = "${file(var.user_data)}"
  key_name               = "${var.keyname}"
  vpc_security_group_ids = ["${aws_security_group.dockerswarm.*.id}"]
  tags {
    Name   = "${var.instance_name_w}-${count.index}"
    Swarm = "Worker"
  }
}

